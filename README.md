# ics-ans-role-lsc

Ansible role to install LSC.

## Role Variables

[defaults/main.yml](defaults/main.yml)


## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lsc
```

## License

BSD 2-clause
