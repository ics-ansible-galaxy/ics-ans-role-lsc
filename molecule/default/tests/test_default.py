import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('lsc')


def test_lsc_sync(host):
    host.run("yum install -y openldap-clients")
    host.run("source /etc/profile && JAVA_OPTS='-Djavax.net.ssl.trustStore=/etc/pki/ca-trust/extracted/java/cacerts' /usr/bin/lsc -s all -c all -f /etc/lsc/")
    cmd = host.run("ldapsearch -H ldaps://ics-ans-role-lsc-ldap-target -x -b 'dc=esss,dc=lu,dc=se' -s sub '(uid=testuser)'")
    assert cmd.succeeded
    assert "# numEntries: 1" in cmd.stdout


def test_lsc_crontab(host):
    cmd = host.run("crontab -lu lsc")
    assert cmd.succeeded
    assert "*/15 * * * * /usr/bin/lsc -s all -c all -f /etc/lsc/" in cmd.stdout
